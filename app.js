
/**
 * Module dependencies
 */

var express   = require('express'),
    routes    = require('./routes'),
    api       = require('./routes/api'),
    http      = require('http'),
    path      = require('path');
    nodeCache = require('node-cache');

var app   = module.exports = express();

cache = new nodeCache();

/**
 * Configuration
 */

// all environments
app.set('port', process.env.PORT || 3001);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.static(path.join(__dirname, 'public')));
app.use(app.router);

// development only
if (app.get('env') === 'development') {
  app.use(express.errorHandler());
}

// production only
if (app.get('env') === 'production') {
  // TODO
};


/**
 * Routes
 */

// serve index
app.get('/', routes.index);

// serve character page
app.get('/character/:name', routes.character);

// serve character page
app.get('/character', routes.character);

// serve characters page
app.get('/characters', routes.characters);

// serve characters page
app.get('/planetresidents', routes.planetresidents);

// serve load characters to cache page
//app.get('/loadcharacterstocache', routes.loadcharacterstocache);

// JSON API
app.get('/api/name', api.name);

// redirect all others to the index (HTML5 history)
//app.get('*', routes.index);

/**
/* Gettings all the characters records in order to be able for doing some
/* data sorting
 **/

function loadCache( newPage ){

  page = 1;

  if( newPage ){
    page = newPage;
  }


  var host = 'swapi.co';
  var path = '/api/people/?page=' + page;

  var options = {
    host:    host,
    path:    path
  };

  var req = http.request(options, function(res) {
      var data = "";
      res.on("data", function(msg) {
          data += msg.toString("utf-8");
      });
      res.on("end", function() {

          var history = JSON.parse(data);


          // Getting the planet residents

          var planet = "";
          var person = "";

          var planetResidents = [];

          if( cache.get( "planetresidents" ) ){
            planetResidents = cache.get( "planetresidents" );
          }

          for( var i = 0; i < history.results.length; i++ ){

            //console.log( "Planet: " );
            //console.log( "planet_" + history.results[ i ].homeworld.match( /[0-9]+/g )[ 0 ] );
            //console.log( "Character: " + history.results[ i ].name );

            planet = "planet_" + history.results[ i ].homeworld.match( /[0-9]+/g )[ 0 ];
            person = history.results[ i ].name;

            if( planetResidents[ planet ] !== undefined ){
              planetResidents[ planet ].push( person );
            }else{
              planetResidents[ planet ] = [ person ];
            }

          }

          //console.log( planetResidents );
          cache.set( "planetresidents", planetResidents, 0 );

          console.log( "planetresidents cache: " );
          console.log( cache.get( "planetresidents" ) );

          // Getting the characters cache

          var prev_content = JSON.stringify( history.results ).substring(1);
          var content      = prev_content.substring( 0, prev_content.length - 1)
          var old_content  = "";
          var new_content  = "";

          console.log( "Next Page:" );
          console.log( history.next );
          //console.log( "Results:" );
          //console.log( content );

          if( cache.get( "characters" ) ){
              old_content = cache.get( "characters" );
          }

          old_content.length > 0 ? new_content = old_content + "," + content : new_content = content;

          cache.set( "characters", new_content, 0 );

          if( history.next ){
            console.log( "Calling loadCache with page: " + page++ );

            if( page <= 5){
              loadCache( page++ );
            }else{
              console.log( "" );
              console.log( "We got the cache of the 50 star wars characters" );
              //console.log( "" );
              //console.log( cache.get( "characters" ) );
            }

          }

      });
  });
  req.end();

  req.on('error', function(e) {
      // Decide what to do here
      // if error is recoverable
      //     tryUntilSuccess(options, callback);
      // else
      //     callback(e);
  });

}

loadCache();

/**
 * Start Server
 */

http.createServer(app).listen(app.get('port'), function () {
  console.log('Express server listening on port ' + app.get('port'));
});
