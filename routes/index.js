var http  = require( "http" );

/*
 * GET home page.
 */

exports.index = function(req, res){
  res.render('index');
};

/*
 * GET all the planet residentes.
 */

exports.planetresidents = function(req, res){

  var simpleArray = [];

  var content = cache.get( "planetresidents" );

  for (var k in content ){
    if ( content.hasOwnProperty(k)) {
         //console.log( "Key is " + k + ", value is" + content[ k ][0] );
         simpleArray.push( '{' + '"' + k + '"' +  ':' + '"' + content[ k ].join( "\," ) + '"' + "}" );
    }
  }

  console.log( "simpleArray" );
  console.log( simpleArray.join( "," ) );

  res.render('planetresidents', { data: '[' + simpleArray.join( "," ) + ']' } );

};

/*
 * GET characters page.
 */

exports.characters = function(req, res){

  var sorting  = "name";
  var viewType = "raw";

  if( req.query.sort ){
    sorting = req.query.sort;
  }

  if( req.query.viewtype ){
    req.query.viewtype !== "raw" ? viewType = "ejs": viewType = "raw" ;
  }

  var content = JSON.parse( "[" + cache.get( "characters" ) + "]" );

  switch( sorting ) {
      case "height":
          content.sort(function(a, b) {

            var heightA = 0;
            var heightB = 0;

            if( a.height !== "unknown" ){ var heightA = parseInt( a.height ) }
            if( b.height !== "unknown" ){ var heightB = parseInt( b.height ) }

            var nameA = heightA; // ignore upper and lowercase
            var nameB = heightB; // ignore upper and lowercase

            return nameA - nameB;

          });
          break;
      case "mass":
          content.sort(function(a, b) {
            var massA = 0;
            var massB = 0;

            if( a.mass !== "unknown" ){ var massA = parseInt( a.mass ) }
            if( b.mass !== "unknown" ){ var massB = parseInt( b.mass ) }

            var nameA = massA; // ignore upper and lowercase
            var nameB = massB; // ignore upper and lowercase

            return nameA - nameB;
          });
          break;
      default:
        content.sort( function( a, b ) {
          var nameA = a.name.toUpperCase(); // ignore upper and lowercase
          var nameB = b.name.toUpperCase(); // ignore upper and lowercase
          if (nameA < nameB) {
            return -1;
          }
          if (nameA > nameB) {
            return 1;
          }

          // names must be equal
          return 0;
        });
  }

  //console.log( content );

  res.render('characters', { charactersList: content, viewtype: viewType, sorting: sorting } );

};

/*
 * GET character page.
 */

exports.character = function(req, res){

  var name = 'luke';

  if( req.params.name ){
    name = req.params.name;
  }

  var host = 'swapi.co';
  var path = '/api/people/?search=' + name;

  http.get({
      host: host,
      path: path
  }, function(response) {
      // Continuously update stream with data
      var body = '';
      response.on('data', function(d) {
          body += d;
      });
      response.on('end', function() {

          // Data reception is done, do whatever with it!
          var parsed = JSON.parse(body);
          console.log( parsed );
          res.render('character', parsed );
      });
  });

};
